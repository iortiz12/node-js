# Repositorio Curso: Desarrollo del lado servidor: NodeJS, Express y MongoDB

Repositorio para tareas de curso Desarrollo del lado servidor: NodeJS, Express y MongoDB, se tendra branch por semana a presentar, las cuales se haran merge a master una vez aprobadas.

# Semana 3

Adición de autenticación y autorización.
Adición mailer.
Adición passport.

Se crea documento donde se evidencia correcto funcionamiento de los cambios realizados, [aqui](./doc/Semana_3.docx)

# Datos correo electronicos pruebas
Name:	Tyrique Dickens
Username:	tyrique.dickens@ethereal.email (also works as a real inbound email address)
Password:	ETSazCbH9MZd4BUWUW
